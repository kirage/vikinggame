﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

public class CountdownToStart : MonoBehaviour
{
    private float countdownLength = 3;

    void Start()
    {
        if(GameManager.Instance.gameState == GameState.Countdown)
        {
            StartCoroutine(GameUIManager.Instance.StartCountDown(countdownLength));
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using UnityEngine.SceneManagement;
using Types;

public class ControllerPair : MonoBehaviour
{
    private const string gameScene = "GameTommi";
    [SerializeField]
    public static List<Controller> controllers = new List<Controller>();
    //[SerializeField]
    //private string gameScene = "Game";
    public ReInput.PlayerHelper playerList;
    public GameObject characterSelection;
    public Transform parent;
    public GameObject shadeOutImage;
    public Dictionary<GameObject, Inputs> characterSelectionInputs;
    public Dictionary<Inputs, bool> inputsAssigned;
    public Inputs[] availableInputs = new Inputs[10];
    private int assignInputPointer = 0;
    public int playersJoined = 0;

    private void Awake()
    {
        controllers.Clear();
    }

    // Start is called before the first frame update
    void Start()
    {
        inputsAssigned = new Dictionary<Inputs, bool>();
        foreach (Controller controller in ReInput.controllers.Controllers)
        {
            if (controller.name != "Mouse")
            {
                controllers.Add(controller);
            }
        }
        PopulateInputs();
        EventManager.TriggerEvent("InputsCreated");
    }

    void PopulateInputs()
    {
        playerList = ReInput.players;
        int j = 0;
        Debug.Log("playerList count: " + playerList.playerCount);
        for (int i = 0; i < playerList.playerCount; i++)
        {
            if (i < controllers.Count)
            {
                playerList.GetPlayer(i).controllers.AddController(controllers[i], true);

                Debug.Log($"Controller {controllers[i]} added to player {playerList.GetPlayer(i).name}");
                Inputs input = gameObject.AddComponent<Inputs>();
                input.setPlayerId(i);

                if (GameManager.Instance.gameState == GameState.JoinGame)
                {
                    availableInputs[i] = input;
                    inputsAssigned[input] = false;
                }
            }
        }
    }

    private void Update()
    {
        // Instantiating new characterSelection when any button is pressed in controller for the first time
        for (int i = 0; i < inputsAssigned.Keys.Count; i++)
        {
            Inputs inputs = availableInputs[i];
            if (inputs.AnyButtonDown() && !inputsAssigned[inputs] && playersJoined < 4)
            {
                playersJoined++;
                // Assigning inputs to characterSelection
                inputsAssigned[inputs] = true;
                GameObject newCharacterSelection = Instantiate(characterSelection, parent);
                //newCharacterSelection.transform.SetAsFirstSibling();
                newCharacterSelection.GetComponent<CharacterSelection>().inputs = inputs;
            }
        }
        // Scenen nimi viittaa peli skenen nimeen
        if (Input.GetKeyDown(KeyCode.F6) && GameManager.Instance.gameState == GameState.JoinGame)
        {
            if (GameManager.Instance.playerInputs.Count <= 1)
            {
                GameManager.Instance.gameState = GameState.Testing;
            }
            else
            {
                GameManager.Instance.gameState = GameState.Countdown;
            }
            GameManager.Instance.SendControllers(controllers);
            SceneManager.LoadScene(gameScene);
        }
    }

    public static void LoadGame()
    {
        if (GameManager.Instance.gameState == GameState.JoinGame)
        {
            if (GameManager.Instance.playerInputs.Count <= 1)
            {
                GameManager.Instance.gameState = GameState.Testing;
            }
            else
            {
                GameManager.Instance.gameState = GameState.Countdown;
            }
            AudioManager.Instance.PlaySFX("Battlecry");
            GameManager.Instance.SendControllers(controllers);
            SceneManager.LoadScene(gameScene);
        }
    }
}

%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: LeftArm
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: VIKING
    m_Weight: 1
  - m_Path: vikingRig
    m_Weight: 0
  - m_Path: vikingRig/IKTarget_arm.L
    m_Weight: 1
  - m_Path: vikingRig/IKTarget_arm.R
    m_Weight: 1
  - m_Path: vikingRig/IKTarget_leg.L
    m_Weight: 0
  - m_Path: vikingRig/IKTarget_leg.R
    m_Weight: 0
  - m_Path: vikingRig/PoleTarget_arm.L
    m_Weight: 1
  - m_Path: vikingRig/PoleTarget_arm.R
    m_Weight: 1
  - m_Path: vikingRig/PoleTarget_leg_L
    m_Weight: 0
  - m_Path: vikingRig/PoleTarget_leg_R
    m_Weight: 0
  - m_Path: vikingRig/spine
    m_Weight: 0
  - m_Path: vikingRig/spine/pelvis.L
    m_Weight: 0
  - m_Path: vikingRig/spine/pelvis.R
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.L
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L.002
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L.004
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L.006
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L.008
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.R
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R.002
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R.004
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R.006
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R.008
    m_Weight: 1
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.005
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.005_L
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.005_L/spine.005_L.001
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.005_R
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.005_R/spine.005_R.001
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.006_L
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.006_L/spine.006_L.001
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.006_R
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.006_R/spine.006_R.001
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.007
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.002/spine.003/spine.004/spine.007/spine.008
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.009
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.009_L
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.009_L.001
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.009_L.001/spine.009_L.002
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.009_R
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.009_R.001
    m_Weight: 0
  - m_Path: vikingRig/spine/spine.001/spine.009_R.001/spine.009_R.002
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.L
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.L/shin.L
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.L/shin.L/foot.L
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.L/shin.L/foot.L/heel.02.L
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.L/shin.L/foot.L/toe.L
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.R
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.R/shin.R
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.R/shin.R/foot.R
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.R/shin.R/foot.R/heel.02.R
    m_Weight: 0
  - m_Path: vikingRig/spine/thigh.R/shin.R/foot.R/toe.R
    m_Weight: 0

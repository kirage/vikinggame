﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

public class Jab : MonoBehaviour
{

    [SerializeField]
    private int jabDamage = 0;
    public GameObject collidedObject;
    private Player player;

    private void Start()
    {
        player = transform.parent.GetComponent<Player>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject == collidedObject)
        //{
        //    return;
        //}
        collidedObject = other.gameObject;
        Debug.Log("Jab hit " + other.gameObject.name);

        // Hitting player
        if (collidedObject.tag == "Player" && collidedObject != transform.parent.gameObject)
        {
            ParticleManager.Instance.PlayEffect(other.gameObject, "JabHitPlayerParticle");
            Player jabbedPlayer = collidedObject.GetComponent<Player>();
            jabbedPlayer.playerSounds.PlaySFX("Jab hit", 0.15f);

            if (jabbedPlayer.movementState != Movement.Stunned)
            {
                jabbedPlayer.TakeHit(transform.parent.transform, jabDamage);

                if (player.TaggedWithDeath)
                {
                    jabbedPlayer.TagWithDeath(true);
                    player.TagWithDeath(false);
                }
                //Sets player's direction towards hitting  player
                jabbedPlayer.transform.LookAt(transform.parent.transform);
                Vector3 v = transform.rotation.eulerAngles;
                jabbedPlayer.transform.rotation = Quaternion.Euler(0, v.y + 180, 0);
            }
        }

        #region HittingObject
        // Hitting pickup object

        //else if (collidedObject.tag == "Throwable")
        //{
        //    //TODO fix iscolliding player reference
        //   PickUp pickUp = collidedObject.gameObject.GetComponent<PickUp>();
        //    switch (pickUp.itemState)
        //    {
        //        case ItemState.Ground:
        //            pickUp.BreakItem();
        //            break;
        //        case ItemState.Held:
        //            pickUp.DetachPickUp();
        //            break;
        //        case ItemState.Air:
        //            pickUp.BreakItem();
        //            break;
        //        default:
        //            break;
        //    }
        //    player.collidingObject = null;
        //    //player.playerState = PlayerState.Idle;
        //    //player.pickUpObject = null;
        //    player.isColliding = false;
        //}
        #endregion
    }
}

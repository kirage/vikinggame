﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAudio : MonoBehaviour
{

    public void PlaySelected()
    {
        UIAudioManager.instance.PlaySelected();
    }

    public void PlayClick()
    {
        UIAudioManager.instance.PlayClicked();
    }

    public void PlayValueChange()
    {
        UIAudioManager.instance.PlayValueChanged();
    }

}

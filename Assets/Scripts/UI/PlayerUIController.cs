﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIController : MonoBehaviour
{
    //TODO> make singleton instance
    public static PlayerUIController instance;
    public GameObject playerUITop;

    [SerializeField]
    private Sprite[] playerImages = null;

    void Start()
    {
        StartCoroutine(AddPlayersToUI());
        //AddPlayersToUI();
        instance = this;
    }

    //Add player's images along the healthbars to GameUI.
    IEnumerator AddPlayersToUI()
    {
        yield return new WaitForSeconds(0.1f);
        GameManager gameManager = GameManager.Instance;
        int i = 0;
        int playerAmount = gameManager.playerList.Count;
        foreach (Player player in gameManager.playerList.Keys)
        {
            Debug.Log($"Player's {player.name} id is {gameManager.playerList[player]}");
            int playerId = gameManager.playerList[player];
            int characterIndex = gameManager.playerCharacters[playerId];
            Sprite playerTexture = playerImages[characterIndex];
            GameObject newPlayerUI = Instantiate(playerUITop, this.transform);
            Image playerIcon = newPlayerUI.transform.GetChild(0).GetComponent<Image>();
            i++;
            // Mirror image horizontally depending on player amount
            switch (playerAmount)
            {
                case 2:
                    if (i == 2)
                    {
                        playerIcon.rectTransform.localScale = new Vector3(-1, 1, 1);
                    }
                    break;
                case 4:
                    if (i == 3 || i == 4)
                    {
                        playerIcon.rectTransform.localScale = new Vector3(-1, 1, 1);
                    }
                    break;
                default:
                    break;
            }

            newPlayerUI.GetComponent<PlayerUI>().SetPlayerImage(playerTexture);
            newPlayerUI.GetComponent<PlayerUI>().player = player;
            //newPlayerUI.transform.SetAsFirstSibling();
        }
    }
}





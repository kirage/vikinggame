﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Types;
using TMPro;
using Rewired.Integration.UnityUI;
using Rewired;

public class GameUIManager : MonoBehaviour
{
    private static GameUIManager instance;
    public GameObject winPopUp;
    public GameObject pauseMenu;
    public GameObject gamePausedMenu;
    public GameObject optionsMenu;
    public GameObject victoryScreen;
    public Image winnerImage;
    public GameObject resumeButton;
    public GameObject musicSlider;
    public GameObject restartButton;
    public static bool GameIsPaused = false;
    public GameObject CountDownObject;

    private ScoreUI scoreUI;

    private RewiredStandaloneInputModule rewiredStandaloneInputModule;

    [SerializeField]
    private AudioClip lastDrumHit = null;

    // With public instance GameUIManager can be referenced from anywhere.
    public static GameUIManager Instance
    {
        get
        {
            return instance;
        }
    }

    // GameUIManager implements singleton pattern.
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Debug.Log("Two GameUIManagers, destroying the other.");
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        rewiredStandaloneInputModule = GameObject.FindGameObjectWithTag("EventSystemUI").GetComponent<RewiredStandaloneInputModule>();
    }

    private void Start()
    {
        GameManager.Instance.gameUIManager = this;
        scoreUI = winPopUp.GetComponent<ScoreUI>();
        pauseMenu.SetActive(false);

        if (rewiredStandaloneInputModule.RewiredInputManager == null)
        {
            rewiredStandaloneInputModule.RewiredInputManager = GameObject.FindGameObjectWithTag("RWInputManager").GetComponent<InputManager_Base>();
        }
    }


    //Toggles WinPanel on/off
    // TODO: Change to Menupanel
    public void TogglePause()
    {
        if (GameManager.Instance.gameState == GameState.Victory || GameManager.Instance.gameState == GameState.Win)
        {
            return;
        }
        if (GameIsPaused)
        {
            if (optionsMenu.activeInHierarchy)
            {
                GamePaused();
            }
            else
            {
                Resume();
            }
        }
        else
        {
            GamePaused();
        }
    }

    public void RoundWon(int playerId = 0)
    {
        if (scoreUI == null)
        {
            scoreUI = winPopUp.GetComponent<ScoreUI>();
        }
        winPopUp.SetActive(true);
        
        scoreUI.setWinMessage($"Player {GameManager.Instance.playerNumbers[playerId]}");
        scoreUI.gameObject.SetActive(true);

        scoreUI.AddOnePoint(playerId);
    }

    public void Victory(int playerId)
    {
        StartCoroutine(VictoryDelayed(2f, playerId));
        //StartCoroutine(SelectedButtonDelayed(1f));
    }

    IEnumerator VictoryDelayed(float delay, int playerId)
    {
        yield return new WaitForSeconds(delay);
        AudioManager.Instance.StopMusic();
        Time.timeScale = 0f;
        winPopUp.SetActive(false);
        victoryScreen.SetActive(true);
        int characterIndex = GameManager.Instance.playerCharacters[playerId];
        winnerImage.sprite = scoreUI.GetCharacterImage(characterIndex);
        //EventSystem.current.SetSelectedGameObject(restartButton);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(restartButton);
        //yield return new WaitForSeconds(1f);
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    public void GamePaused()
    {
        Time.timeScale = 0f;
        GameIsPaused = true;
        //pauseMenu.SetActive(true);
        
        pauseMenu.SetActive(true);
        gamePausedMenu.SetActive(true);
        optionsMenu.SetActive(false);

        //EventSystem.current.SetSelectedGameObject(resumeButton);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(resumeButton);

        //resumeButton.GetComponent<Animator>().SetTrigger("Selected");
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        AudioManager.Instance.PlayMusic("BackgroundGame");
        GameManager.Instance.ClearLists();
        GameManager.Instance.ClearPoints();
        GameManager.Instance.RestartGame();
    }

    public void GoToOptions()
    {
        Debug.Log("Showing Options");
        optionsMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(musicSlider);
        gamePausedMenu.SetActive(false);
    }



    public void GoToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
        GameManager.Instance.ResetAllLists();
        GameManager.Instance.gameState = GameState.MainMenu;
    }

    public IEnumerator StartCountDown(float length)
    {
        CountDownObject.SetActive(true);
        TextMeshProUGUI numberText = CountDownObject.GetComponentInChildren<TextMeshProUGUI>();
        AudioSource audioSource = CountDownObject.GetComponentInChildren<AudioSource>();
        Animator anim = CountDownObject.GetComponentInChildren<Animator>();
        bool numberChanged = false;
        int lastnumber = 5;

        float timer = length;
        while (timer >= 0)
        {
            if(timer <= 3 && lastnumber > 3)
            {
                ChangeNumber(3);
                lastnumber = 3;
            }
            else if(timer <= 2 && lastnumber > 2)
            {
                ChangeNumber(2);
                lastnumber = 2;
            }
            else if(timer <= 1 && lastnumber > 1)
            {
                ChangeNumber(1);
                lastnumber = 1;
            }
            timer -= Time.deltaTime;
            yield return null;
        }

        anim.SetTrigger("Scaledown");
        audioSource.PlayOneShot(lastDrumHit);
        numberText.text = "Go!";
        StartCoroutine(DisableGameObject(1f, CountDownObject));

        if (GameManager.Instance.gameState == GameState.Countdown)
        {
            GameManager.Instance.gameState = GameState.Game;
        }
    }

    private void ChangeNumber(int number)
    {
        TextMeshProUGUI numberText = CountDownObject.GetComponentInChildren<TextMeshProUGUI>();
        AudioSource audioSource = CountDownObject.GetComponentInChildren<AudioSource>();
        Animator anim = CountDownObject.GetComponentInChildren<Animator>();

        if (numberText.text != number.ToString())
        {
            anim.SetTrigger("Scaledown");
            audioSource.Play();
            numberText.text = number.ToString();
        }

    }

    private IEnumerator DisableGameObject(float time, GameObject toInactivate)
    {
        yield return new WaitForSeconds(time);

        toInactivate.SetActive(false);
    }
}

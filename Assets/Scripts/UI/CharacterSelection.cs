﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterSelection : MonoBehaviour
{
    public Sound[] sounds;
    public Inputs inputs;
    private Image characterImage;
    private UICharacterJoin characterJoin;
    private TextMeshProUGUI hintText;
    private int index = 0;
    private int previousIndex = 0;
    private bool playerJoined = false;
    private bool stickPressed = false;
    private bool characterSelected = false;
    private int characterAmount;
    private Dictionary<int, bool> lockedCharacters;
    private AudioSource audioSource;

    //private void Awake()
    //{

    //}

    void Start()
    {
        //inputs = GetComponent<Inputs>();
        //inputs = transform.parent.GetComponent<ControllerPair>().characterSelectionInputs[gameObject];
        lockedCharacters = new Dictionary<int, bool>();
        characterImage = GetComponentInChildren<Image>();
        hintText = GetComponentInChildren<TextMeshProUGUI>();
        //characterImage.enabled = false;
        characterJoin = transform.parent.GetComponent<UICharacterJoin>();
        characterAmount = characterJoin.characterSprites.Length;
        lockedCharacters = characterJoin.lockedCharacters;
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        CheckInputs();
    }

    private void SelectCharacter()
    {
        if (lockedCharacters[index])
        {
            return;
        }
        characterJoin.lockedPlayers++;
        PlayAudio("Select");
        characterSelected = true;
        hintText.text = "Ready!";
        //ChangeImageDarkness(0.2f);
        GameManager.Instance.AddInputsToList(inputs, inputs.playerId);
        GameManager.Instance.AssignCharacter(inputs.playerId, index);
        lockedCharacters[index] = true;
    }

    private void DeselectCharacter()
    {
        characterJoin.lockedPlayers--;
        PlayAudio("Deselect");
        characterSelected = false;
        hintText.text = "";
        //ChangeImageDarkness(1f);
        GameManager.Instance.RemoveInputsFromList(inputs, inputs.playerId);
        lockedCharacters[index] = false;
    }

    private void ChangeImageDarkness(float amount)
    {
        characterImage.color = new Vector4(amount, amount, amount, 1);
    }

    private void LateUpdate()
    {
        //Changing image darkness if character chosen by some player
        if (lockedCharacters[index])
        {
            ChangeImageDarkness(0.2f);
        }
        else
        {
            ChangeImageDarkness(1f);
        }
    }


    private void CheckInputs()
    {
        // Selecting and deselecting character
        if (inputs.SelectInput() && !characterSelected)
        {
            SelectCharacter();
        }
        else if (inputs.CancelInput())
        {
            DeselectCharacter();
        }

        // Changing player character
        if (characterSelected)
        {
            return;
        }

        if (inputs.MoveInput.x > 0.1f && !stickPressed)
        {
            stickPressed = true;
            PlayAudio("MoveRight");
            index++;
        }
        else if (inputs.MoveInput.x < -0.1f && !stickPressed)
        {
            stickPressed = true;
            PlayAudio("MoveLeft");
            index--;
        }
        else if (Mathf.Abs(inputs.MoveInput.x) < 0.1f && stickPressed)
        {
            stickPressed = false;
        }
        index = (index + characterAmount) % characterAmount;
        characterImage.sprite = characterJoin.characterSprites[index];

    }

    private void PlayAudio(string name)
    {
        foreach (Sound sound in sounds)
        {
            if (sound.name == name)
            {
                audioSource.PlayOneShot(sound.clip);
            }
        }
    }



}

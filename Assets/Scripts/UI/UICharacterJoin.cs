﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using Rewired;
using UnityEngine.UI;
using System.Collections.Concurrent;
using TMPro;

public class UICharacterJoin : MonoBehaviour
{
    //[SerializeField]
    //private GameObject playerCharacterImage = null;

	
    public Sprite[] characterSprites = null;

    public Dictionary<int, bool> lockedCharacters;

    [SerializeField]
    private TextMeshProUGUI startButtonMessage = null;

    private UnityAction playerJoinDelegate;

    [HideInInspector]
    public int lockedPlayers = 0;
    public TextMeshProUGUI addControllersText;
    public TextMeshProUGUI pressStartText;

    
    void Awake()
    {
        playerJoinDelegate += PlayerJoin;
    }

    private void Start()
    {
        lockedCharacters = new Dictionary<int, bool>(4);
        for (int i = 0; i < 4; i++)
        {
            lockedCharacters.Add(i, false);
        }
    }

    private void OnEnable()
    {
        EventManager.StartListening("PlayerJoin", playerJoinDelegate);
    }

    private void OnDisable()
    {
        EventManager.StopListening("PlayerJoin", playerJoinDelegate);
    }

    public void PlayerJoin()
    {
        if(GameManager.Instance.playerInputs.Count >= 2)
        {
            startButtonMessage.enabled = true;
        }
        else
        {
            startButtonMessage.enabled = false;
        }
    }

    private void Update()
    {
        if (lockedPlayers >= GameManager.Instance.requiredPlayers)
        {
            pressStartText.enabled = true;
            addControllersText.enabled = false;
        } else
        {
            pressStartText.enabled = false;
            addControllersText.enabled = true;
        }
    }

}

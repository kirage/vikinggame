﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.UI;

public class UIAudioPlayer : MonoBehaviour
{
    public AudioClip[] audioClips;
    private AudioSource audioSource;
    public GameObject input;
    private Inputs[] inputs;
    private UnityAction UIAudioDelegate;
    private GameObject currentSelected;

    private void Awake()
    {
        UIAudioDelegate += FetchInputs;
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        currentSelected = EventSystem.current.currentSelectedGameObject;
    }

    void FetchInputs()
    {
        inputs = input.GetComponents<Inputs>();
    }

    void OnEnable()
    {
        EventManager.StartListening("InputsCreated", UIAudioDelegate);
    }

    void OnDisable()
    {
        EventManager.StopListening("InputsCreated", UIAudioDelegate);
    }

    private void Update()
    {
        foreach (Inputs input in inputs)
        {
            //Inputs ownInput = this.ownInput;
            if (input.MoveInput.y > 0.1f)
            {
                PlaySFX("MoveUp");
            }
            else if (input.MoveInput.y < -0.1f)
            {
                PlaySFX("MoveDown");
            }
            else if (input.UseInput)
            {
                PlaySFX("Submit");
            }
            else if (input.StartInput)
            {
                PlaySFX("Cancel");
            }
        }
    }

    public void PlaySFX(string name)
    {
        if (currentSelected != EventSystem.current.currentSelectedGameObject)
        {
            currentSelected = EventSystem.current.currentSelectedGameObject;
            Debug.Log("Playing UI SFX");
            audioSource.PlayOneShot(audioSource.clip);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Types;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    private UICharacterJoin uICharacterJoin;
    public float delay = 1f;
    private string sceneName = "JoinPlayer";
    public GameObject mainmenu;
    public GameObject options;
    public GameObject credits;
    public GameObject startButton;
    public GameObject musicSlider;
    public GameObject backButton;
    public TextMeshProUGUI pointsToWinText;

    private void Start()
    {
        ChangePointsToWin(0);
    }


    public void LoadPlayerJoinScene()
    {
        GameManager.Instance.gameState = GameState.JoinGame;
        StartCoroutine(LoadSceneAfterDelay(sceneName, delay));
    }

    public void LoadGameScene()
    {
        GameManager.Instance.gameState = GameState.Countdown;
        SceneManager.LoadScene("Game");
    }

    public void MainMenu()
    {
        EventSystem.current.SetSelectedGameObject(startButton);
    }

    public void Options()
    {
    }

    public void Credits()
    {
        EventSystem.current.SetSelectedGameObject(backButton);
    }

    public void ChangePointsToWin(int increaseAmount)
    {
        GameManager.Instance.pointsToWin = (GameManager.Instance.pointsToWin + increaseAmount) % 6;
        pointsToWinText.text = $" Points to win \n {GameManager.Instance.pointsToWin} ";
    }

    public void QuitGame()
    {
        Debug.Log("Quitting game...");
        StartCoroutine(QuitAfterDelay(delay));
    }

    IEnumerator QuitAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Application.Quit();
    }

    IEnumerator LoadSceneAfterDelay(string sceneName, float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(sceneName);
    }



}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIAudioManager : MonoBehaviour
{
    public static UIAudioManager instance = null;

    public AudioClip select;
    public AudioClip click;
    public AudioClip valueChange;

    private AudioSource audioSource;
    private bool clickPlaying = false;
    private bool playSelected = false;


    void Awake()
    {
        //If there is not already an instance of SoundManager, set it to this.
        if (instance == null)
        {
            instance = this;
        }
        //If an instance already exists, destroy whatever this object is to enforce the singleton.
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySelected()
    {
        playSelected = true;
    }

    public void PlayClicked()
    {
        clickPlaying = true;
        audioSource.PlayOneShot(click);
    }

    public void PlayValueChanged()
    {
        audioSource.PlayOneShot(valueChange);
    }

    private void LateUpdate()
    {
        if (playSelected)
        {
            if (clickPlaying)
            {
                clickPlaying = false;
            }
            else
            {
                audioSource.PlayOneShot(select);
            }
        }
        playSelected = false;
    }
}

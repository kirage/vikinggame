﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public Image playerImage;
    public Player player;
    public int score;

    public void SetPlayerImage(Sprite image)
    {
        playerImage.sprite = image;
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.InputSystem.XR.Haptics;

public class ScoreUI : MonoBehaviour
{
	[SerializeField]
	private TMP_Text tmpWinMessage = null;

	[SerializeField]
	private Sprite[] characterImages;

	[SerializeField]
	private GameObject playerScoreOne = null;

	[SerializeField]
	private GameObject playerScoreTwo = null;

	[SerializeField]
	private GameObject playerScoreThree = null;

	[SerializeField]
	private GameObject playerScoreFour = null;

	private List<GameObject> playerScores = new List<GameObject>();
	private string winMessage = "Won this round!";

	private void Awake()
	{
		playerScores.Add(playerScoreOne);
		playerScores.Add(playerScoreTwo);
		playerScores.Add(playerScoreThree);
		playerScores.Add(playerScoreFour);
	}

	private void Start()
	{
		AssignCharacterImages();
		AssignScores();
	}

	public void setWinMessage(string playerName)
	{
		tmpWinMessage.text = $"{playerName} {winMessage}";
		gameObject.transform.GetChild(0).gameObject.SetActive(true);
	}

	private void AssignCharacterImages()
	{
		//for(int i = 0; i < GameManager.Instance.playerCharacters.Count; i++)
		//{
		//	playerScores[i].SetActive(true);
		//	playerScores[i].transform.GetChild(0).GetComponent<Image>().sprite = characterImages[GameManager.Instance.playerCharacters[i]];
		//}
		foreach (int playerId in GameManager.Instance.playerCharacters.Keys)
		{
			//int i = playerId;
			int i = GameManager.Instance.playerCharacters[playerId];
			playerScores[i].SetActive(true);
			playerScores[i].transform.GetChild(0).GetComponent<Image>().sprite = characterImages[GameManager.Instance.playerCharacters[playerId]];
		}
	}

	private void AssignScores()
	{
		foreach (int playerId in GameManager.Instance.playerCharacters.Keys)
		{
			int i = GameManager.Instance.playerCharacters[playerId];
			playerScores[i].SetActive(true);
			playerScores[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = $"{GameManager.Instance.scoreList[playerId]}";
		}
	}

	public void AddOnePoint(int playerId)
	{
		// siirrä metodi kutsut startista pois, kun scoreui activoi tämän niin ne tapahtuu aina ensin.
		int i = GameManager.Instance.playerCharacters[playerId];
		playerScores[i].transform.GetChild(1).GetComponent<Animator>().SetTrigger("ScoreUp");
		GameManager.Instance.scoreList[playerId] += 1;
	}

	public Sprite GetCharacterImage(int i)
	{
		return characterImages[i];
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;
using UnityEngine.Events;

public class Game : MonoBehaviour
{
    public GameObject currentHitObject;
    Vector3 spawnPos;
    float spawnTimer;

    [SerializeField]
    float spawnDuration = 0.5f;

    [SerializeField]
    float barrelpercentage = 50;
    [SerializeField]
    float bottlepercentage = 20;
    [SerializeField]
    float stoolpercentage = 30;


    float newSpawnDuration;

    public GameObject throwable;
    public GameObject arena;
    GameObject spawnObj;
    private List<Vector3> spawnedObjPositions;
    public List<GameObject> activePlayers;
    public List<GameObject> listOfThrowables;

    float xRangeMin;
    float xRangeMax;
    float zRangeMin;
    float zRangeMax;
    float arenaSize;
    int activePlayersAmount;

    public float spawnTime = 0.1f;

    [SerializeField, Range(5, 10)]
    int nodes = 10;

    float nodeWidth;
    private float halfNode;

    int objectsOnArena = 0;

    bool gameBegin = true;

    [SerializeField]
    private int maxThrowablesOnArena = 50;

    public LayerMask layerMask;

    int itemTypes = System.Enum.GetValues(typeof(Types.ItemType)).Length;

    private UnityAction objectPickedUpDelegate;

    private Player timedDeathPlayer;

    [SerializeField]
    private GameObject PowerUpPrefab = null;

    [SerializeField]
    private float powerUpSpawnTime = 5f;

    private float powerUpTimer = 0;

    [HideInInspector]
    public bool powerUpActive = false;

    Quaternion randomRotation;

    bool isAboveArena = false;

    void Awake()
    {
        objectPickedUpDelegate += DecreaseObjectCount;
        if (AudioManager.Instance != null)
        {
            if (AudioManager.Instance.currentMusicSource != AudioManager.Instance.FindMusicWithName("BackgroundGame"))
            {
                AudioManager.Instance.PlayMusic("BackgroundGame");
            }
        }
    }

    private void OnEnable()
    {
        EventManager.StartListening("ObjectPicked", objectPickedUpDelegate);
    }

    private void OnDisable()
    {
        EventManager.StopListening("ObjectPicked", objectPickedUpDelegate);
    }

    void Start()
    {
        newSpawnDuration = spawnDuration;
        spawnTimer = spawnDuration;

        arenaSize = Mathf.RoundToInt((arena.GetComponent<BoxCollider>().size.x) * 10); // 184
        SplitArenaToNodes();
        arenaSize = arenaSize / 10f;

        xRangeMin = Mathf.RoundToInt(arena.transform.position.x - (arenaSize / 2f));
        xRangeMax = Mathf.RoundToInt(arena.transform.position.x + (arenaSize / 2f));
        zRangeMin = Mathf.RoundToInt(arena.transform.position.z - (arenaSize / 2f));
        zRangeMax = Mathf.RoundToInt(arena.transform.position.z + (arenaSize / 2f));

    }

    Vector3 GetRandomPosition()
    {
        int randomXPos = Random.Range(1, nodes + 1);
        int randomYPos = Random.Range(1, nodes + 1);

        Vector3 v = new Vector3(xRangeMin + randomXPos * nodeWidth - halfNode, 0f, zRangeMin + randomYPos * nodeWidth - halfNode);

        return v;
    }

    private void SpawnFirstThrowables()
    {
        bool placeIsFree = true;

        for (int i = 0; i < activePlayersAmount; i++)
        {
            placeIsFree = false;

            while (!placeIsFree)
            {
                spawnPos = GetRandomPosition();
                RaycastHit hit;

                if (Physics.Raycast(spawnPos - new Vector3(0, 0.5f, 0), transform.TransformDirection(Vector3.up), out hit, Mathf.Infinity, layerMask))
                {
                    currentHitObject = hit.transform.gameObject;
                    Debug.DrawRay(spawnPos, transform.TransformDirection(Vector3.up) * 5f, Color.red);
                }
                else
                {
                    Debug.DrawRay(spawnPos, transform.TransformDirection(Vector3.up) * 5f, Color.white);
                    spawnObj = Instantiate(throwable, spawnPos, randomRotation) as GameObject;
                    isAboveArena = true;
                    placeIsFree = true;
                }
            }
        }    
        gameBegin = false;
    }

    void Update()
    {
        if (GameManager.Instance.gameState == GameState.Game || GameManager.Instance.gameState == GameState.Countdown)
        {
            activePlayers = GameManager.Instance.activePlayers;
        }
        activePlayersAmount = activePlayers.Count;

        // Spawns throwables in beginning. Amount is player amount.
        if (gameBegin)
        {
            SpawnFirstThrowables();
        }

        if (spawnObj != null)
        {
            spawnObj.transform.position = Vector3.Lerp(spawnObj.transform.position, spawnPos, spawnTime);
            if (Mathf.Approximately(spawnObj.transform.position.magnitude, spawnPos.magnitude))
            {
                isAboveArena = true;
            }
        } else
        {
            isAboveArena = true;
        }

        // Adjusts the spawn duration to player amount
        newSpawnDuration = spawnDuration / activePlayersAmount;

        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0 && objectsOnArena < maxThrowablesOnArena && isAboveArena)
        {
            DefineThrowableType();

            Vector3 oldSpawnPos = spawnPos;
            spawnPos = GetRandomPosition();
            randomRotation = Quaternion.Euler(0, Random.Range(0, 160), 0);
            Vector3 spawnPosUnderground = spawnPos - new Vector3(0, 2f, 0);

            RaycastHit hit;

            if (Physics.Raycast(spawnPos - new Vector3(0, 0.5f, 0), transform.TransformDirection(Vector3.up), out hit, Mathf.Infinity, layerMask))
            {
                currentHitObject = hit.transform.gameObject;
                Debug.DrawRay(spawnPos, transform.TransformDirection(Vector3.up) * 5f, Color.red);
                //Debug.Log($"Did Hit {currentHitObject}");
                spawnPos = oldSpawnPos;
            }
            else
            {
                Debug.DrawRay(spawnPos, transform.TransformDirection(Vector3.up) * 5f, Color.white);
                spawnObj = Instantiate(throwable, spawnPosUnderground, randomRotation) as GameObject;
                isAboveArena = false;
                objectsOnArena++;
            }

            spawnTimer = newSpawnDuration;
        }

        HandlePowerUpSpawn();
    }

    void DefineThrowableType()
    {
        float maxPercentage = barrelpercentage + bottlepercentage + stoolpercentage;

        int throwableType = Random.Range(1, 101);

        if (throwableType > 0 && throwableType <= barrelpercentage)
        {
            throwable = listOfThrowables[0];
        }
        if (throwableType > barrelpercentage && throwableType <= (barrelpercentage + bottlepercentage))
        {
            throwable = listOfThrowables[1];
        }
        if (throwableType > (barrelpercentage + bottlepercentage) && throwableType <= maxPercentage)
        {
            throwable = listOfThrowables[2];
        }
    }

    void DecreaseObjectCount()
    {
        objectsOnArena--;
    }

    void SplitArenaToNodes()
    {
        nodeWidth = (arenaSize / 10f) / nodes;
        halfNode = nodeWidth / 2f;
    }

    private void OnDrawGizmos()
    {
        float maxDistance = 5f;
        RaycastHit hit;

        bool isHit = Physics.SphereCast(spawnPos, halfNode, transform.TransformDirection(Vector3.up), out hit, maxDistance, layerMask, QueryTriggerInteraction.UseGlobal);

        if (isHit)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(spawnPos, transform.TransformDirection(Vector3.up));
            Gizmos.DrawWireSphere(spawnPos, halfNode);
        }
    }

    public void SetTimedDeathPlayer(Player player)
    {
        timedDeathPlayer = player;

        Debug.Log(timedDeathPlayer + "Tagged with Death!");
    }

    public IEnumerator StartTimedDeath(Player player, float duration)
    {
        float timer = 0;
        timedDeathPlayer = player;
        while (timer <= duration)
        {
            timer += Time.deltaTime;
            //Debug.Log(timer);
            yield return null;
        }

        timedDeathPlayer.TakeDamage(10);
        Debug.Log("Timed death dealth 10 damage to " + timedDeathPlayer);
    }

    private void HandlePowerUpSpawn()
    {
        if (!powerUpActive)
        {
            powerUpTimer += Time.deltaTime;
            if (powerUpSpawnTime <= powerUpTimer)
            {
                Vector3 pickUpSpawnPos = GetRandomPosition();
                Vector3 spawnPosAir = pickUpSpawnPos + new Vector3(0, 1f, 0);

                RaycastHit hit;

                if (Physics.Raycast(pickUpSpawnPos - new Vector3(0, 0.5f, 0), transform.TransformDirection(Vector3.up), out hit, Mathf.Infinity, layerMask))
                {
                    //currentHitObject = hit.transform.gameObject;
                    Debug.DrawRay(pickUpSpawnPos, transform.TransformDirection(Vector3.up) * 5f, Color.red);
                }
                else
                {
                    Debug.DrawRay(pickUpSpawnPos, transform.TransformDirection(Vector3.up) * 5f, Color.white);
                    GameObject pickUpObj = Instantiate(PowerUpPrefab, spawnPosAir, PowerUpPrefab.transform.rotation);

                    powerUpTimer = 0;
                    powerUpActive = true;
                }
            }

        }
    }
}

﻿using UnityEngine;
using Types;
using System;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
    #region Unity properties

    [SerializeField, Range(0, 50)]
    private float walkSpeed = 8.0f;

    [SerializeField, Range(0, 50)]
    private float airControlSpeed = 6.0f;

    [SerializeField, Range(0, 1)]
    private float airControlPercent = 0.1f;

    [SerializeField, Range(0, 50)]
    private float jumpSpeed = 8.0f;

    [SerializeField, Range(0, 20)]
    private float gravityMultiplier = 2f;

    [SerializeField, Range(0, 20)]
    private float keepOnGroundForce = 2f;

    [SerializeField, Range(0, 1f)]
    private float groundDistanceTolerance = 0.1f;

    [SerializeField, Range(0, 1f)]
    private float holdMovementSpeedPercent = 0.8f;

    [SerializeField]
    private GameObject runningTrails = null;

    [SerializeField]
    private GameObject tornadoEffect = null;

    [SerializeField]
    private GameObject walkingSmokeTrails = null;

    #endregion

    #region Movement state variables

    private Vector3 velocity = Vector3.zero;

    public Vector3 Velocity
    {
        get => velocity;
        set => velocity = value;
    }

    private Vector3 lastFrameVelocity;

    public bool IsGrounded { get; private set; }

    private Vector2 input = Vector2.zero;

    private bool hitCeiling;
    private CollisionFlags collisionFlags;
    private bool isWalking;
    private bool isJumping;
    public LayerMask collideLayers;

    private Vector3 rayPositionOffset = Vector3.zero;

    private Vector3 targetRotation;

    private float movementSpeedModifier = 0f;
    private float movementBuffDuration = 0f;
    #endregion

    #region Components

    //private GameInput gameInput;
    private CharacterController characterController;
    private Player player;
    private Inputs inputs;

    #endregion

    #region Unity events

    private void Start()
    {
        characterController = GetComponent<CharacterController>();

        inputs = GetComponent<Inputs>();
        player = GetComponent<Player>();
    }

    private void Update()
    {
        float speed = GetInput();

        Transform tr = transform;

        Vector3 targetMovement = Vector3.forward * input.y + Vector3.right * input.x;

        rayPositionOffset.y = characterController.height / 2f;

        // Check if there's something solid close below us
        bool didHit = Physics.SphereCast(tr.position + rayPositionOffset,
            characterController.radius * 0.8f, Vector3.down, out var hit,
            rayPositionOffset.y + groundDistanceTolerance, collideLayers, QueryTriggerInteraction.Ignore);

        float hitAngle = Vector3.Angle(Vector3.up, hit.normal);

        // Adjust direction based on ground slope, but only if the slope isn't too steep
        if (hitAngle < characterController.slopeLimit)
        {
            targetMovement = Vector3.ProjectOnPlane(targetMovement, hit.normal).normalized;
        }
        else
        {
            targetMovement.Normalize();
        }

        // Apply movement only when on (or near) ground and player isn't moving upwards
        if ((characterController.isGrounded && didHit) && velocity.y <= 0f)
        {          
            IsGrounded = true;
            velocity.x = targetMovement.x * speed;
            velocity.z = targetMovement.z * speed;

            // Apply a bit of force to help keep us grounded
            if (characterController.isGrounded) velocity.y = -keepOnGroundForce;

            if (inputs.JumpInput && GameManager.Instance.gameState != GameState.Countdown)
            {
                velocity.y = jumpSpeed;
                gameObject.GetComponentInChildren<Animator>().SetTrigger("Jump");
                isJumping = true;
            }

            if (!walkingSmokeTrails.activeInHierarchy)
            {
                walkingSmokeTrails.SetActive(true);
            }
        }
        else
        {
            if (walkingSmokeTrails.activeInHierarchy)
            {
                walkingSmokeTrails.SetActive(false);
            }

            IsGrounded = false;

            Vector3 airVelocity = new Vector3(input.x * airControlSpeed, velocity.y, input.y * airControlSpeed);

            velocity = Vector3.Lerp(lastFrameVelocity, airVelocity, airControlPercent);
        }
        // If player is holding something reduce movement speed.
        if(player.playerState == PlayerState.Hold && player.movementState != Movement.Jumping)
        {
            velocity.x *= holdMovementSpeedPercent;
            velocity.z *= holdMovementSpeedPercent;
        }

        // Apply constant gravity to also help keep us grounded
        velocity += gravityMultiplier * Time.deltaTime * Physics.gravity;

        collisionFlags = characterController.Move(velocity * Time.deltaTime);

        targetRotation.x = transform.position.x + velocity.x * 100;
        targetRotation.z = transform.position.z + velocity.z * 100;
        targetRotation.y = transform.position.y;

        lastFrameVelocity = velocity;
        if (characterController.isGrounded && isJumping)
        {
            isJumping = false;
            gameObject.GetComponentInChildren<Animator>().SetTrigger("Land");
            ParticleManager.Instance.PlayEffect(gameObject, "LandingDust");
        }

        if (!IsGrounded)
        {
            player.movementState = Movement.Jumping;
        }
        else if (Mathf.Abs(velocity.x) < 0.1 && Mathf.Abs(velocity.z) < 0.1)
        {
            player.movementState = Movement.Idle;
        }
        else
        {
            player.movementState = Movement.Moving;
        }

        if (input != Vector2.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(targetRotation - transform.position);
            transform.rotation = Quaternion.Lerp(transform.rotation, toRotation, 10f * Time.deltaTime);
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if ((collisionFlags & CollisionFlags.Below) == CollisionFlags.Below)
        {
            hitCeiling = false;
            return;
        }

        // Stop upwards velocity if we hit something above, and reset when we return to ground
        if ((collisionFlags & CollisionFlags.Above) != CollisionFlags.Above || hitCeiling ||
            velocity.y <= 0) return;

        hitCeiling = true;
        velocity.y = 0;
    }

    #endregion

    #region Movement

    public float GetInput()
    {
        if (GameManager.Instance.gameState == GameState.Countdown) return 0;

        input.x = inputs.MoveInput.x;
        input.y = inputs.MoveInput.y;

        if (input.sqrMagnitude > 1)
            input.Normalize();

        isWalking = player.activePowerUps.ContainsKey("SpeedUp");

        return isWalking ? walkSpeed + (walkSpeed * movementSpeedModifier) : walkSpeed;
    }

    public void ResetMovement()
    {
        velocity = Vector3.zero;
        input = Vector2.zero;
    }

	#endregion

	#region Misc Methods
    public void SetMovementSpeedBuffValues(float modifier, float duration)
    {
        movementSpeedModifier = modifier;
        movementBuffDuration = duration;
    }

    public void StartSpeedUp()
    {
        StartCoroutine(StartTimerForBuffDuration());
    }

    public IEnumerator StartTimerForBuffDuration()
    {
        runningTrails.SetActive(true);
        tornadoEffect.SetActive(true);
        float timer = 0;
        while (timer <= movementBuffDuration)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        Debug.Log("Buff ended");
        player.activePowerUps.Remove("SpeedUp");
        runningTrails.SetActive(false);
        tornadoEffect.SetActive(false);
    }
    #endregion Misc Methods
}

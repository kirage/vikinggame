﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Types;

public class Player : MonoBehaviour
{
    private Inputs inputs;
    private Game gameHandler;
    public GameObject pickUpObject;
    public GameObject collidingObject;
    public Transform hand;
    public Jab jab;
    public GameObject healthCanvas;
    public bool overheadHealthbarOn = false;
    public bool isColliding;

    [SerializeField]
    private SphereCollider jabCollider = null;
    [SerializeField]
    private float jabTime = 0.1f;

    public float jabCoolDown = 0.5f;
    private float time = 0;
    private bool jabStarted = false;
    public float useInputCoolDown = 0.25f;
    private bool useInputPressed = false;

    public PlayerSounds playerSounds;
    public AudioSource walkingAudioSource;
    public PlayerState playerState;
    public Movement movementState = Movement.Idle;
    public Animator animator;
    public GameObject shieldChild;

    private Rigidbody objectRigidbody;
    private PickUp pickUp;

    [SerializeField]
    private GameObject cloudEffect = null;
    [SerializeField]
    private GameObject[] extraDamageAura = null;

    //Taking hit parameters
    //private Vector3 hitStartPosition;
    //private Vector3 hitEndPosition;
    public float hitDuration = 3f;
    public float hitRiseAmount = 1f;
    //public float hitPushAmount = 1f;
    //public float hitPower = 1f;
    //private Transform hitTransform;

    public Dictionary<string, int> activePowerUps = new Dictionary<string, int>();
    public Health Health
    {
        get;
        private set;
    }

    public bool TaggedWithDeath
    {
        get;
        private set;
    }

    private void Awake()
    {
        Health = GetComponent<Health>();
        TaggedWithDeath = false;
        gameHandler = GameObject.FindGameObjectWithTag("GameHandler").GetComponent<Game>();
        if (Health == null)
        {
            Debug.LogError($"Health component could not be found from {this.name}");
        }
        jabCollider.gameObject.SetActive(false);
        healthCanvas.SetActive(overheadHealthbarOn);
    }

    private void Start()
    {
        inputs = gameObject.GetComponent<Inputs>();
        animator = gameObject.GetComponentInChildren<Animator>();
        playerSounds = GetComponentInChildren<PlayerSounds>();
        AddActivePlayer();
    }

    private void AddActivePlayer()
    {
        if (GameManager.Instance.gameState == GameState.Testing)
        {
            GameManager.Instance.activePlayers.Add(this.gameObject);
        }
    }

    public void HandleJab()
    {
        if (playerState == PlayerState.Idle)
        {
            playerSounds.PlaySFX("Jab");
            playerState = PlayerState.Jab;
            //animator.SetTrigger("Jab");
        }
    }

    private void Update()
    {
        CheckInputs();

        if (playerState == PlayerState.Jab && !jabStarted)
        {
            StartCoroutine(Jab());
        }
        if (Health.CurrentHealth <= Health.MinHealth)
        {
            //Die();
        }
        if (movementState == Movement.Stunned)
        {
            MoveByHit();
        }
        if (movementState == Movement.Idle || movementState == Movement.Moving && movementState != Movement.Dead)
        {
            GetComponent<PlayerMovement>().enabled = true;
        }
        // Resolving bugs in playerstates:
        // If player has reference to pickupobject but is left in idle state
        if (playerState != PlayerState.Hold && pickUpObject != null)
        {

            pickUpObject = null;
        }
        // If player is in hold state but there's no pickup object
        if (playerState == PlayerState.Hold && pickUpObject == null)
        {
            animator.SetBool("isPickingUp", false);
            playerState = PlayerState.Idle;
        }

        CheckStates();
    }

    private void CheckStates()
    {
        switch (movementState)
        {
            case Movement.Idle:
                animator.SetBool("isWalking", false);
                break;
            case Movement.Jumping:
                //animator.SetTrigger("Jump");
                break;
            case Movement.Moving:
                animator.SetBool("isWalking", true);
                if (activePowerUps.ContainsKey("SpeedUp"))
                {
                    animator.SetFloat("walkSpeedMultiplier", 2);
                }
                else
                {
                    animator.SetFloat("walkSpeedMultiplier", 1);
                }
                
                break;
            case Movement.Stunned:
                break;
        }
        SetWalkingAudio();
    }

    private void SetWalkingAudio()
    {
        walkingAudioSource.enabled = movementState == Movement.Moving;
    }

    //Calculate position where player moves when is hit by an opponent.
    public void TakeHit(Transform otherTransform, int damage)
    {
        inputs.PlayVibration(0.2f, Mathf.Clamp(damage * 0.2f, 0.3f, 1f));
        TakeDamage(damage);
        //GetComponent<PlayerMovement>().enabled = false;

        if (playerState == PlayerState.Hold)
        {
            pickUp.DetachPickUp();
        }
        //hitTransform = otherTransform;
        //Vector3 pushDirection = transform.position - hitTransform.position;
        //hitEndPosition = pushDirection * hitPower;
    }

    //Moves player's transform to hitEndPosition in hitDuration time.
    private void MoveByHit()
    {
        time += Time.deltaTime;
        if (time > hitDuration)
        {
            time = 0;
            if (movementState != Movement.Dead)
            {
                GetComponent<PlayerMovement>().enabled = true;
                movementState = Movement.Idle;
            }

        }
        else
        {
            //Hit rises player up and back down
            float angle = time / hitDuration * Mathf.PI;
            float up = Mathf.Sin(angle) * hitRiseAmount;
            transform.position = new Vector3(transform.position.x, up, transform.position.z);
        }
    }

    //Makes jab collider active and changes playerstate to Jab for jabTime seconds.
    IEnumerator Jab()
    {
        animator.SetTrigger("Jab");
        jabStarted = true;
        jabCollider.gameObject.SetActive(true);
        yield return new WaitForSeconds(jabTime);
        jabCollider.gameObject.SetActive(false);
        yield return new WaitForSeconds(jabCoolDown);
        if (playerState == PlayerState.Jab)
        {
            playerState = PlayerState.Idle;
            jabStarted = false;
        }
    }


    private void CheckInputs()
    {
        if (GameManager.Instance.gameState != GameState.Countdown || movementState != Movement.Dead)
        {
            if (inputs.UseInput && !useInputPressed)
            {
                HandlePickUp();
            }
            else if (inputs.AttackInput)
            {
                HandleJab();
            }
        }


        if (inputs.StartInput)
        {
            Debug.Log("game Paused by " + gameObject.name);
            GameUIManager.Instance.TogglePause();
        }

        if (inputs.RestartInput)
        {
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            GameManager.Instance.gameState = GameState.JoinGame;
            SceneManager.LoadScene("JoinGame");
        }
    }

    IEnumerator UseInputCoolDown(string inputName)
    {
        useInputPressed = true;
        yield return new WaitForSeconds(useInputCoolDown);
        useInputPressed = false;
    }

    public void HandlePickUp()
    {
        StartCoroutine(UseInputCoolDown(""));
        if (playerState == PlayerState.Idle && isColliding && pickUpObject == null && collidingObject != null)
        {
            if (collidingObject.GetComponent<PickUp>().itemState == ItemState.Held)
            {
                return;
            }
            animator.SetTrigger("PickUp");
            animator.SetBool("isPickingUp", true);
            PickThrowable();
        }
        else if (playerState == PlayerState.Hold && pickUpObject != null)
        {
            animator.SetBool("isPickingUp", false);
            //StartCoroutine(Throw());
            Throw();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        AssignCollidingObject(other);
    }
    void OnTriggerStay(Collider other)
    {
        AssignCollidingObject(other);
    }

    public void AssignCollidingObject(Collider other)
    {
        if (other.gameObject.tag == "Throwable")
        {
            if (other.gameObject.GetComponent<PickUp>().itemState == ItemState.Ground)
            {
                isColliding = true;
                collidingObject = other.gameObject;
            }
        }
    }

    //Reference to collidingObject is removed when player exits it's collider
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Throwable")
        {
            if (other.gameObject.GetComponent<PickUp>().itemState == ItemState.Ground)
            {
                isColliding = false;
                collidingObject = null;
            }
        }
    }

    //The damage player takes is handled
    public void TakeDamage(int amount)
    {
        //EventManager.TriggerEvent("Damage");
        playerSounds.PlaySFX("Hurt");
        if (Health.DecreaseHealth(amount))
        {
            Die();
        }

        //if (!Health.IsImmortal && Health.CurrentHealth >= 1 && !activePowerUps.ContainsKey("Shield"))
        //{
        //    Health.MakeImmortal();
        //    StunPlayer();
        //}
    }

    public void StunPlayer()
    {
        if (movementState != Movement.Dead)
        {
            movementState = Movement.Stunned;
            animator.SetTrigger("Stun");
            inputs.stopGettingInput = true;
            inputs.ResetInput();
            StartCoroutine(CountdownToMovementStateChange(Movement.Idle, Health.stunDuration));
        }
    }

    public void PickThrowable()
    {
        if (isColliding)
        {
            playerSounds.PlaySFX("PickUp");
            playerState = PlayerState.Hold;
            pickUpObject = collidingObject;
            //if (pickUpObject != null)
            //{
            //    objectRigidbody = pickUpObject.GetComponent<Rigidbody>();
            //}
            pickUp = pickUpObject.GetComponent<PickUp>();

            pickUp.ParentPlayer(this.gameObject);
            //objectRigidbody.velocity = Vector3.zero;
            //objectRigidbody.angularVelocity = Vector3.zero;
            pickUpObject.transform.parent = hand;

            pickUp.itemState = ItemState.Held;
            isColliding = false;
        }
    }

    public void Throw()
    {
        if (pickUpObject != null)
        {
            playerState = PlayerState.Idle;
            playerSounds.PlaySFX("Throw");

            pickUpObject.transform.parent = null;
            pickUp.Unfreeze();

            if (activePowerUps.ContainsKey("ExtraDamage"))
            {
                pickUp.damage += activePowerUps["ExtraDamage"];
                pickUp.ChangeTrails();
                activePowerUps.Remove("ExtraDamage");
                ActivateExtraDamageEffect(false);
            }

            //Object moves along the parabola.
            pickUpObject.GetComponent<ParabolaController>().FollowParabola();
            pickUp.itemState = ItemState.Air;
            pickUpObject = null;

            //yield return new WaitForSeconds(throwCoolDown);
            //playerState = PlayerState.Idle;
            //isColliding = false;
        }
    }

    private void Die()
    {
        inputs.Player.StopVibration();
        GameManager.Instance.activePlayers.Remove(gameObject);
        animator.SetTrigger("Death");
        StartCoroutine(DisablePlayer());
    }

    public void SetInput(Inputs input)
    {
        this.inputs = input;
    }

    public Inputs GetInput()
    {
        return inputs;
    }

    private IEnumerator CountdownToMovementStateChange(Movement state, float duration)
    {
        float timer = 0;
        while (timer <= duration)
        {
            timer += Time.deltaTime;
            //Debug.Log(timer);
            yield return null;
        }
        movementState = state;
        if (movementState != Movement.Stunned)
        {
            inputs.stopGettingInput = false;
            animator.SetBool("IsStunned", false);
        }
    }

    public void TagWithDeath(bool death)
    {
        TaggedWithDeath = death;

        if (death)
        {
            gameHandler.SetTimedDeathPlayer(this);
            cloudEffect.SetActive(true);
        }
        else
        {
            cloudEffect.SetActive(false);
        }
    }

    public void ActivateShield()
    {
        shieldChild.SetActive(true);

        ShieldEffect shield = shieldChild.GetComponent<ShieldEffect>();

        shield.CheckActiveShields();
    }

    public void DeactivateShield()
    {
        shieldChild.SetActive(false);
    }

    public void ActivateExtraDamageEffect(bool active)
    {
        for (int i = 0; i < extraDamageAura.Length; i++)
        {
            if (i == 0)
            {
                extraDamageAura[i].SetActive(active);
            }
            else
            {
                Component halo = extraDamageAura[i].GetComponent("Halo");
                halo.GetType().GetProperty("enabled").SetValue(halo, active, null);
            }
        }

    }

    private IEnumerator DisablePlayer()
    {
        movementState = Movement.Dead;

        GetComponent<PlayerMovement>().enabled = false;
        GetComponent<CharacterController>().enabled = false;
        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 3.0f);
        Debug.Log("player disabled");
        gameObject.SetActive(false);
        ParticleManager.Instance.PlayEffect(gameObject, "Die");
    }
}

﻿using UnityEngine;
using Rewired;
using RewiredConsts;
using System.Collections;
using UnityEngine.SceneManagement;
using Types;

public class Inputs : MonoBehaviour
{
    [SerializeField]
    private int testingSceneControllerId = 0;

    public int playerId;

    public bool vibrationOn = true;

    [HideInInspector]
    public bool stopGettingInput = false;

    public Rewired.Player Player { private set; get; }

    public Controller controller { private set; get; }

    public Vector2 MoveInput { get; set; }

    public bool JumpInput { get; set; }

    public bool UseInput { get; set; }

    public bool AttackInput { get; set; }

    public bool StartInput { get; set; }

    public bool RestartInput { get; set; }

    private void Start()
    {
        if (GameManager.Instance.gameState == Types.GameState.Testing)
        {
            if (GameManager.Instance.playerInputs.Count < 1)
            {
                setPlayerId(testingSceneControllerId);
            }
        }
    }

    private void Update()
    {
        if (Player == null) return;

        if (!stopGettingInput)
        {
            GetInput();
        }

        if (GameManager.Instance.gameState == Types.GameState.JoinGame)
        {
            if ((StartInput) && GameManager.Instance.playerInputs.Contains(this) && GameManager.Instance.playerInputs.Count >= GameManager.Instance.requiredPlayers)
            {
                // TODO check that game can be started only if all connected players are added
                if (GetComponent<UICharacterJoin>().lockedPlayers == GetComponent<ControllerPair>().playersJoined)
                {
                    ControllerPair.LoadGame();
                }
            }
            //if (JumpInput)
            //{
            //    GameManager.Instance.ResetAllLists();
            //    GameManager.Instance.gameState = GameState.MainMenu;
            //    SceneManager.LoadScene("MainMenu");
            //}
        }
    }

    private void GetInput()
    {
        float horizontal = Player.GetAxis(Action.HorizontalMove);
        float vertical = Player.GetAxis(Action.VerticalMove);
        MoveInput = new Vector2(horizontal, vertical);

        JumpInput = Player.GetButtonDown(Action.Jump);
        UseInput = Player.GetButtonDown(Action.Use);
        AttackInput = Player.GetButtonDown(Action.Attack);

        StartInput = Player.GetButtonDown(Action.Start);
        //if (StartInput)
        //{
        //    Debug.Log("Startinput by " + gameObject.name);
        //}
        RestartInput = Player.GetButtonDown(Action.Restart);

    }

    public bool CancelInput()
    {
        // TODO> Make Cancel action and put to this
        return Player.GetButtonDown(Action.Use) || Player.GetButtonDown(Action.Jump);
    }

    public bool SelectInput()
    {
        // TODO> Make Select action and put to this
        return Player.GetButtonDown(Action.Use) || Player.GetButtonDown(Action.Jump);
    }

    public bool AnyButtonDown()
    {
        return Player.GetAnyButtonDown();
    }

    public void ResetInput()
    {
        float horizontal = 0;
        float vertical = 0;

        MoveInput = new Vector2(horizontal, vertical);

        JumpInput = false;
        UseInput = false;
        AttackInput = false;

        StartInput = false;
        RestartInput = false;
    }

    public void AddController()
    {
        Player.controllers.AddController(GameManager.Instance.playerControllers[Player.id], true);
    }

    public void setPlayerId(int id)
    {
        playerId = id;
        Player = ReInput.players.GetPlayer(playerId);
    }

    public void PlayVibration(float time, float motorLevel)
    {
        if (vibrationOn)
        {
            StartCoroutine(Vibrate(time, motorLevel));
        }
    }

    public void StopVibration()
    {
        if (vibrationOn)
        {
            Player.StopVibration();
        }
    }

    IEnumerator Vibrate(float time, float motorLevel)
    {
        Player.SetVibration(0, motorLevel);
        yield return new WaitForSeconds(time);
        Player.StopVibration();
    }
}

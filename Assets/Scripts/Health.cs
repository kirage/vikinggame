﻿using System;
using System.Collections;
using UnityEngine;

public class Health : MonoBehaviour, IHealth
{
    [SerializeField]
    private int startHealth = 10;
    [SerializeField]
    private int maxHealth = 10;
    [SerializeField]
    private int minHealth = 0;

    [SerializeField]
    public float stunDuration = 0.2f;
    [SerializeField]
    public float immortalityDuration = 0.5f;

    private new Renderer renderer;
    private Player player;

    public int CurrentHealth
    {
        get;
        private set;
    }

    public int MaxHealth
    {
        get { return maxHealth; }
    }

    public int MinHealth
    {
        get { return minHealth; }
    }
    public bool IsImmortal
    {
        get;
        private set;
    }

    void Start()
    {
        Reset();
        renderer = GetComponentInChildren<Renderer>();
        player = GetComponent<Player>();
    }

    public bool DecreaseHealth(int amount)
    {
        if (IsImmortal == false)
        {
            if (player.activePowerUps.ContainsKey("Shield"))
            {
                if(player.activePowerUps["Shield"] <= 1)
                {
                    player.activePowerUps.Remove("Shield");
                    player.ActivateShield();
                    MakeImmortal();
                }
                else
                {
                    player.activePowerUps["Shield"] -= 1;
                    player.ActivateShield();
                    MakeImmortal();
                }
            }
            else
            {
                CurrentHealth = Mathf.Max(CurrentHealth - amount, MinHealth);

                if(CurrentHealth >= 1)
                {
                    MakeImmortal();
                    player.StunPlayer();
                }
            }
        }

        return CurrentHealth <= MinHealth;
    }

    public void IncreaseHealth(int amount)
    {
        CurrentHealth = Mathf.Min(CurrentHealth + amount, MaxHealth);
    }

    public void Reset()
    {
        //CurrentHealth = Mathf.Clamp(startHealth, MinHealth, MaxHealth);
        CurrentHealth = startHealth;
    }

    public void MakeImmortal()
    {
        IsImmortal = true;

        // päättää kuinka kauan renderer on disabled ja enabled
        StartCoroutine(BlinkRenderer(0.2f));
    }

    private IEnumerator BlinkRenderer(float blinkLength)
    {
        int numBlinks = (int)(immortalityDuration / blinkLength);

        for (int i = 0; i < numBlinks; i++)
        {

            //toggle renderer
            renderer.enabled = !renderer.enabled;

            //wait for a bit
            yield return new WaitForSeconds(blinkLength);
        }

        //make sure renderer is enabled when we exit
        renderer.enabled = true;
        IsImmortal = false;
    }
}

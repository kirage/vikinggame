﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

public class HealthBar : MonoBehaviour
{
    [SerializeField, Tooltip("Väri, kun hit pointit ovat täysissä")]
    private Color fullColor = Color.green;

    [SerializeField, Tooltip("Väri, kun hit pointit ovat minimissä")]
    private Color emptyColor = Color.red;

    [SerializeField]
    [Tooltip("Läpinäkyvyys: 0 - täysin läpinäkyvä, 1 - läpinäkymätön")]
    private float alpha = 1f;

    private Image image;
    private float maxWidth;
    private Gradient gradient;
    Quaternion iniRot;

    public PlayerUI playerUI;
    public bool isPlayerUI = false;

    public Player Owner
    {
        get;
        private set;
    }

    public UnityAction takeDamage;

    private void Awake()
    {
        image = GetComponent<Image>();
        gradient = new Gradient();

        // Gradientin värit (alku- ja loppuväri)
        GradientColorKey[] colorKeys = new GradientColorKey[2];
        // Gradientin läpinäkyvyysarvot (alku- ja loppuarvot)
        GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];

        // Alustetaan alkuarvot
        colorKeys[0].color = emptyColor;
        colorKeys[0].time = 0;
        alphaKeys[0].alpha = 1;
        alphaKeys[0].time = 0;

        // Loppuarvot
        colorKeys[1].color = fullColor;
        colorKeys[1].time = 1;
        alphaKeys[1].alpha = 1;
        alphaKeys[1].time = 1;

        gradient.SetKeys(colorKeys, alphaKeys);

        maxWidth = image.rectTransform.sizeDelta.x;

        //transform.up = transform.up;
        iniRot = transform.rotation;
    }

    private void Start()
    {
        //Corresponding Player reference is fetched whether the Healthbar is in GameUI or in Player prefab.
        if (isPlayerUI)
        {
            Owner = playerUI.player;
        }
        else
        {
            Owner = this.GetComponentInParent<Player>();
            this.GetComponentInParent<Canvas>().gameObject.transform.up = transform.up;
        }
    }


    private void Update()
    {
        float healthPercent = Owner.Health.CurrentHealth / (float)Owner.Health.MaxHealth;

        Vector2 size = image.rectTransform.sizeDelta;
        size.x = maxWidth * healthPercent;
        image.rectTransform.sizeDelta = size;

        Color fullPortion = healthPercent * fullColor;
        Color emptyPortion = (1 - healthPercent) * emptyColor;
        Color currentColor = fullPortion + emptyPortion;
        currentColor.a = alpha;
        image.color = currentColor;
    }

    private void LateUpdate()
    {
        //transform.rotation = iniRot;
        this.GetComponentInParent<Canvas>().gameObject.transform.rotation = iniRot;
    }
}

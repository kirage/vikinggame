﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

public class ParticleManager : MonoBehaviour
{
    //[SerializeField]
    //ParticleSystem[] parts;
    //ParticleSystem[] newparts;
    public GameObject[] effects;

    public GameObject groundHit;
    public GameObject bottleHit;
    public GameObject playerDie;
    public GameObject stoolHit;

    private GameObject particle;
    private static ParticleManager instance;

    public static ParticleManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Debug.Log("Two ParticleManagers, destroying this one.");
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void PlayEffect(GameObject obj)
    {
        // Getting ItemType of gameobject
        Transform effectTransform = obj.transform;
        ItemType objType = obj.GetComponent<PickUp>().itemType;

        // Instantiate corresponding effect.
        switch (objType)
        {
            case ItemType.Barrel:
                particle = Instantiate(groundHit, effectTransform.position, Quaternion.identity);
                break;
            case ItemType.Bottle:
                particle = Instantiate(bottleHit, effectTransform.position, Quaternion.identity);
                break;
            case ItemType.Stool:
                particle = Instantiate(stoolHit, effectTransform.position, Quaternion.identity);
                break;
        }
        particle.AddComponent<Particle>();
    }

    public void PlayEffect(GameObject obj, string effectName)
    {
        foreach (var effect in effects)
        {
            if (effect.name.Equals(effectName))
            {
                particle = Instantiate(effect, obj.transform.position, Quaternion.identity);

                Particle particleComponent = particle.GetComponent<Particle>();

                if (!particleComponent)
                {
                    particle.AddComponent<Particle>();
                }
            }
        }
    }
}

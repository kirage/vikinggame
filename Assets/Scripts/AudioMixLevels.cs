﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioMixLevels : MonoBehaviour
{
    public AudioMixer masterMixer;
    public Slider musicSlider;
    public Slider soundSlider;

    private void Start()
    {
        //SetDefaultValues();
    }
    private void OnEnable()
    {
        //SetSliderValues();   
    }

    private void SetDefaultValues()
    {
        musicSlider.value = 0.5f;
        soundSlider.value = 0.5f;
    }

    public void SetMusicLevels(float lvl)
    {
        float translatedLvl = Mathf.Log10(lvl) * 20;
        Debug.Log($"Music set to {lvl}");
        masterMixer.SetFloat("musicVol", translatedLvl);
    }

    public void SetSfxLevels(float lvl)
    {
        float translatedLvl = Mathf.Log10(lvl) * 20;
        Debug.Log($"Sfx set to {lvl}");
        masterMixer.SetFloat("sfxVol", translatedLvl);
    }

    public void SetSliderValues()
    {
        masterMixer.GetFloat("musicVol", out float musicVol);
        masterMixer.GetFloat("sfxVol", out float sfxVol);
        musicSlider.value = Mathf.Pow(10, musicVol/20);
        soundSlider.value = Mathf.Pow(10, sfxVol / 20);
        //Debug.Log($"Music volume set to {musicVol}");
        //Debug.Log($"SFX volume set to {sfxVol}");
    }

}

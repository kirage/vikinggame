﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;
using UnityEngine.SceneManagement;

public class PlayerLoader : MonoBehaviour
{
    [SerializeField]
    private GameObject[] playerPrefabs = null;
    [SerializeField]
    private Material[] playerColors = null;
    [SerializeField]
    private IList<Transform> playerSpawns = new List<Transform>();

    public static PlayerLoader instance;
    private bool gameSetup = false;
    public bool scoreUpdated = false;

    private void Awake()
    {
        instance = this;
        GameObject PlayerSpawnList = GameObject.FindWithTag("SpawnList");

        for (int i = 0; i < PlayerSpawnList.transform.childCount; i++)
        {
            playerSpawns.Add(PlayerSpawnList.transform.GetChild(i));
        }

        //LoadActivePlayers();
    }

    private void Update()
    {
        if (!gameSetup)
        {
            LoadPlayers();
            gameSetup = true;
        }
    }

    public void LoadPlayers()
    {
        if (GameManager.Instance.playerInputs == null) return;

        if (GameManager.Instance.gameState == GameState.Game || GameManager.Instance.gameState == GameState.Testing || GameManager.Instance.gameState == GameState.Countdown)
        {
            int i = 0;
            foreach (Inputs input in GameManager.Instance.playerInputs)
            {
                int characterIndex = GameManager.Instance.playerCharacters[input.playerId];
                GameObject player = Instantiate(playerPrefabs[characterIndex], playerSpawns[i].position, playerSpawns[i].rotation);

                //ASSIGNING PLAYER COLORS, COMMENTED OUT BECAUSE OF NEW PLAYER MATERIALS
                //SkinnedMeshRenderer playerMR = player.GetComponentInChildren<SkinnedMeshRenderer>();
                //Material[] material = { playerColors[i] };
                //playerMR.materials = material;

                Player playerScript = player.GetComponent<Player>();
                Inputs playerInput = player.GetComponent<Inputs>();
                //playerInput.setPlayerId(GameManager.Instance.playerInputs[characterIndex].playerId);
                playerInput.setPlayerId(input.playerId);
                playerInput.AddController();
                GameManager.Instance.AddPlayerToDictionary(playerScript, input.playerId);
                GameManager.Instance.activePlayers.Add(player);
                GameManager.Instance.playerNumbers[input.playerId] = i + 1;

                if (GameManager.Instance.playerInputs.Count != GameManager.Instance.scoreList.Count)
                {
                    GameManager.Instance.scoreList.Add(playerInput.playerId, 0);
                }
                i++;
            }
        }
    }

    private void LoadActivePlayers()
    {
        if (GameManager.Instance.gameState == GameState.Testing)
        {
            GameManager.Instance.activePlayers.Clear();

            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

            for (int i = 0; i < players.Length; i++)
            {
                GameManager.Instance.activePlayers.Add(players[i]);
            }
        }
    }
}

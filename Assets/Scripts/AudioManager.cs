﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Types;
using UnityEngine.Audio;

public  class AudioManager : MonoBehaviour
{
    //private static AudioManager instance;
    //public static AudioManager Instance
    //{
    //    get
    //    {
    //        if(instance == null)
    //        {
    //            instance = FindObjectOfType<AudioManager>();
    //            if(instance == null)
    //            {
    //                instance = new GameObject("Spawned AudioManager", typeof(AudioManager)).GetComponent<AudioManager>();
    //            }
    //        }
    //        return instance;
    //    }
    //    private set
    //    {
    //        instance = value;
    //    }
    //}

    //private AudioSource musicSource;
    //private AudioSource musicSource2;
    //private AudioSource sfxSource;

    //private bool firstMusicIsPlaying;

    //private void Awake()
    //{
    //    DontDestroyOnLoad(this.gameObject);

    //    musicSource = this.gameObject.AddComponent<AudioSource>();
    //    musicSource2 = this.gameObject.AddComponent<AudioSource>();
    //    sfxSource = this.gameObject.AddComponent<AudioSource>();

    //    musicSource.loop = true;
    //    musicSource2.loop = true;
    //}

    //public void PlayMusic(AudioClip musicClip)
    //{
    //    AudioSource activeSource = (firstMusicIsPlaying) ? musicSource : musicSource2;

    //    activeSource.clip = musicClip;
    //    activeSource.volume = 1;
    //    activeSource.Play();
    //}

    //public void PlaySFX(AudioClip clip)
    //{
    //    sfxSource.PlayOneShot(clip);
    //}

    public GameState gameState;

    public Sound[] sounds;

    
    // Start is called before the first frame update
    [SerializeField]
    public AudioSource currentEffectsSource;
    [SerializeField]
    public AudioSource currentMusicSource;

    [SerializeField]
    public float musicVolume = 5.0f;
    [SerializeField]
    public float sfxVolume = 5.0f;


    // Random pitch adjustment range.
    public float LowPitchRange = .95f;
    public float HighPitchRange = 1.05f;


    [SerializeField]
    public bool musicOn = true;
    [SerializeField]
    public bool sfxOn = true;

    // Singleton instance.
    public static AudioManager Instance = null;
    public AudioMixerGroup musicMixer;

    void Awake()
    {
        //If there is not already an instance of SoundManager, set it to this.
        if (Instance == null)
        {
            Instance = this;
        }
        //If an instance already exists, destroy whatever this object is to enforce the singleton.
        else if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.outputAudioMixerGroup = musicMixer;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    public void SilenceBGMForSeconds(float time)
    {
        StartCoroutine(Silence(time));
    }

    public IEnumerator Silence(float time)
    {
        currentMusicSource.volume *= 0.5f;
        yield return new WaitForSeconds(time);
        currentMusicSource.volume *= 2f;
    }

    public AudioSource GetCurrentMusic()
    {
        return currentMusicSource;
    }
    public AudioSource FindMusicWithName(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        AudioSource backgroundMusicSource = s.source;

        return backgroundMusicSource;
    }


    public void PlaySFX(string name)
    {
        if (sfxOn)
        {
            Sound s = Array.Find(sounds, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogWarning("Sound: " + name + " not found!");
                return;
            }
            s.source.volume = sfxVolume;
            s.source.Play();
            currentEffectsSource = s.source;
        }
    }

    public void PlayMusic(string name)
    {
        if (musicOn)
        {
            Sound s = Array.Find(sounds, sound => sound.name == name);
            if (s == null)
            {
                Debug.LogWarning("Sound: " + name + " not found!");
                return;
            }
            if (currentMusicSource != null)
            {
                currentMusicSource.Stop();
            }
            s.source.volume = musicVolume;
            s.source.Play();
            currentMusicSource = s.source;
        }
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            return;
        }
        s.source.Stop();
    }

    public void StopMusic()
    {
        currentMusicSource.volume = 0f;
    }

    public void StopSFX()
    {
        currentEffectsSource.volume = 0f;
    }

    public void AdjustVolume(float volume)
    {
        currentMusicSource.volume = volume;
        musicVolume = volume;
    }

    public void AdjustSFX(float volume)
    {
        if (currentEffectsSource != null)
        {
            currentEffectsSource.volume = volume;
        }
        sfxVolume = volume;
    }
}

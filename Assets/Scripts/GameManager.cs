﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;
using System;
using UnityEngine.SceneManagement;
using Rewired;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine.SocialPlatforms.Impl;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    private GameObject gameMenu;
    public GameUIManager gameUIManager;

    public Dictionary<int, int> scoreList;
    public int pointsToWin = 2;
    public Dictionary<int, int> playerCharacters;

    // used but not useful
    public Dictionary<Player, int> playerList { private set; get; }
    public Dictionary<int, int> playerNumbers;

    public List<Inputs> playerInputs { private set; get; }
    public List<GameObject> activePlayers;
    public int joinedPlayers = 0;

    public List<Controller> playerControllers { private set; get; }
    // Public instance GameManager can be referenced from anywhere.
    public GameState gameState;
    public bool testing = false;
    public int requiredPlayers = 1;

    private AudioSource currentMusic;

    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    // Gamemanager implements singleton pattern.
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Debug.Log("Two GameManagers, destroying the other");
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        playerInputs = new List<Inputs>();
        playerList = new Dictionary<Player, int>();
        activePlayers = new List<GameObject>();
        scoreList = new Dictionary<int, int>();
        playerCharacters = new Dictionary<int, int>();
        playerNumbers = new Dictionary<int, int>();
        //true if gamestate is Testing at the beginning of the game
        testing = gameState == GameState.Testing;
    }


    private void OnEnable()
    {
        //gameState = GameState.JoinGame;
        SceneManager.sceneLoaded += OnSceneLoaded;

    }

    void Update()
    {
        // Scenen nimi viittaa peli skenen nimeen
        if (gameState == GameState.Game || gameState == GameState.Testing || gameState == GameState.Countdown)
        {
            if (Input.GetKeyDown(KeyCode.F6))
            {
                ClearLists();
                RestartGame();
            }
            if (activePlayers != null)
            {
                // Restarttaa pelin jos pelaajia on hengissä vain 1. ja gamemanagerin gamestate on game
                if (activePlayers.Count == 1 && (gameState == GameState.Game || gameState == GameState.Testing))
                {
                    gameState = GameState.Win;
                    if (!testing)
                    {
                        playerList.Clear();
                    }
                }

                if (gameState == GameState.Win && !PlayerLoader.instance.scoreUpdated)
                {
                    AudioManager.Instance.SilenceBGMForSeconds(3f);
                    AudioManager.Instance.PlaySFX("RoundWon");
                    GameObject WinningPlayer = activePlayers[0];
                    int winningPlayerId = WinningPlayer.GetComponent<Inputs>().playerId;
                    PlayerScoreUp(winningPlayerId);
                    PlayerLoader.instance.scoreUpdated = true;

                    // Jos pelaaja on saanut tarpeeksi voittoja, mene victory ruutuun, muuten restarttaa peli
                    if (scoreList[winningPlayerId] >= pointsToWin)
                    {
                        gameState = GameState.Victory;
                        gameUIManager.Victory(winningPlayerId);
                    }
                    else
                    {
                        RestartGame(3f);
                    }
                }
            }
        }

        currentMusic = AudioManager.Instance.GetCurrentMusic();
    }



    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        //Debug.Log(mode);
        if (!gameUIManager && mode == LoadSceneMode.Single && (gameState == GameState.Game || gameState == GameState.Countdown || gameState == GameState.Testing))
        {
            Debug.Log("GameUI loaded");
            SceneManager.LoadScene("GameUI", LoadSceneMode.Additive);
        }

        if (gameState == GameState.MainMenu)
        {
            AudioManager.Instance.PlayMusic("BackgroundMenu");
        }

        if (gameState == GameState.JoinGame)
        {
            AudioManager.Instance.PlayMusic("Drum");
        }
    }

    public void AddInputsToList(Inputs inputs, int id)
    {
        if (playerInputs == null)
        {
            playerInputs = new List<Inputs>();
        }
        if (!playerInputs.Contains(inputs))
        {
            playerInputs.Add(inputs);
            EventManager.TriggerEvent("PlayerJoin");
        }
    }

    public void RemoveInputsFromList(Inputs inputs, int id)
    {
        if (playerInputs == null)
        {
            playerInputs = new List<Inputs>();
            return;
        }

        if (playerInputs.Contains(inputs))
        {
            playerInputs.Remove(inputs);
            EventManager.TriggerEvent("PlayerJoin");
        }
    }

    public void AssignCharacter(int playerId, int characterIndex)
    {
        if (playerCharacters.ContainsKey(playerId))
        {
            playerCharacters[playerId] = characterIndex;
        }
        else
        {
            playerCharacters.Add(playerId, characterIndex);
        }
    }

    public void RemoveCharacterAssign(int playerId, int characterIndex)
    {
        if (playerCharacters.ContainsKey(playerId))
        {
            playerCharacters.Remove(playerId);
        }
    }

    public void AddPlayerToDictionary(Player player, int id)
    {
        if (playerList == null)
        {
            playerList = new Dictionary<Player, int>();
        }

        if (!playerList.ContainsKey(player))
        {
            playerList.Add(player, id);
        }
    }

    public void SendControllers(List<Controller> controllers)
    {
        playerControllers = controllers;
    }

    public void ClearLists()
    {
        activePlayers.Clear();
        playerList.Clear();
        playerNumbers.Clear();
    }

    public void ResetAllLists()
    {
        activePlayers.Clear();
        playerList.Clear();
        playerInputs.Clear();
        scoreList.Clear();
        playerCharacters.Clear();
        playerControllers.Clear();
        playerNumbers.Clear();
    }

    private bool isRestarting = false;

    public void RestartGame(float delay = 0)
    {
        if (!isRestarting)
        {
            isRestarting = true;
            StartCoroutine(LoadLevelAfterDelay(delay, SceneManager.GetActiveScene().name));
        }
    }

    IEnumerator LoadLevelAfterDelay(float delay, string sceneName)
    {
        yield return new WaitForSeconds(delay);

        ClearLists();

        SceneManager.LoadScene(sceneName);
        isRestarting = false;
        gameState = testing ? GameState.Testing : GameState.Countdown;

        if (AudioManager.Instance.currentMusicSource != AudioManager.Instance.FindMusicWithName("BackgroundGame"))
        {
            AudioManager.Instance.PlayMusic("BackgroundGame");
        }

    }

    public void PlayerScoreUp(int playerId)
    {
        if (!testing)
        {
            // SCOREUI metodia kutsutaan joka kasvattaa voittaneen pistettä, josta triggerataan animaatio.
            GameUIManager.Instance.RoundWon(playerId);
        }
        else
        {
            GameUIManager.Instance.RoundWon();
        }
    }

    public void ClearPoints()
    {
        scoreList.Clear();
    }
}

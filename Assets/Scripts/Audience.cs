﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audience : MonoBehaviour
{
    [SerializeField, Range(3, 50)]
    private float cheerInterval = 5f;

    private Animator anim;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if(timer > cheerInterval)
        {
            anim.SetTrigger("Cheer");
        }
    }
}

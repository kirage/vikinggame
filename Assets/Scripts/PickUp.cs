﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

public class PickUp : MonoBehaviour
{
    public GameObject currentHitObject;


    public GameObject pickUpPlayer;

    public ItemType itemType;
    public ItemState itemState;

    public bool isColliding;

    public bool boxHitsPlayer;
    public bool boxHitsGround;

    [SerializeField]
    private float sphereRadius = 0.8f;
    public LayerMask layerMask;
    public LayerMask groundLayer;

    private Vector3 origin;
    private bool redTrailActive = false;

    //Damage inflicted to player
    [SerializeField]
    public int damage = 1;
    [SerializeField, Range(-50, 50)]
    private int spinSpeed = -10;
    public Vector3 spinAmount = new Vector3(36f, 0, 0);
    public float itemHeight = 1.6f;

    [SerializeField]
    private GameObject trailsPrefab = null;
    [SerializeField]
    private GameObject redtrailsPrefab = null;

    public void Start()
    {
        GetComponent<Rigidbody>().useGravity = true;
    }

    void Update()
    {
        if (itemState == ItemState.Held)
        {
            AdjustTransform();
        }
        origin = transform.position + transform.up * itemHeight / 2;
        if (itemState == ItemState.Air)
        {
            Spin();
            Collider[] hits = Physics.OverlapSphere(origin, sphereRadius, layerMask, QueryTriggerInteraction.UseGlobal);
            if(redTrailActive && !redtrailsPrefab.activeInHierarchy)
            {
                redtrailsPrefab.SetActive(true);
            }
            if (!redTrailActive && !trailsPrefab.activeInHierarchy)
            {
                trailsPrefab.SetActive(true);
            }
            foreach (var hit in hits)
            {
                Debug.Log($"BOX HITS {hit.transform.gameObject.name}");
                // Item hits the player
                if (hit.transform.gameObject.layer == 10 && itemState == ItemState.Air && hit.gameObject != pickUpPlayer)
                {
                    itemState = ItemState.Ground;
                    OnHitsPlayer(hit.gameObject);
                    break;
                }
                // Item hits the ground
                else if (hit.transform.gameObject.layer == 8 && itemState == ItemState.Air) // Layer 8 = ground
                {
                    itemState = ItemState.Ground;
                    OnHitsGround();
                    break;
                }
                // Item hits the other item
                else if (hit.transform.gameObject.tag == "Throwable" && itemState == ItemState.Air &&
                         hit.transform.gameObject != gameObject)
                {
                    itemState = ItemState.Ground;
                    OnHitsObject(hit.gameObject);
                    break;
                }
            }
        }
    }

    private void AdjustTransform()
    {
        if (transform.parent != null)
        {
            transform.position = transform.parent.position;
            transform.rotation = transform.parent.rotation;
            

            if(itemType == ItemType.Stool)
            {
                transform.position = transform.parent.position;
                transform.position += Vector3.up;
                transform.Rotate(new Vector3(270, 0, 0), Space.Self);
            }
            else
            {
                transform.Rotate(new Vector3(90, 0, 0), Space.Self);
            }
        }
    }

    private void Spin()
    {
        transform.Rotate(spinAmount * spinSpeed * Time.deltaTime, Space.Self);
    }

    public void OnHitsPlayer(GameObject other)
    {
        Player hitPlayer = other.GetComponent<Player>();
        hitPlayer.TakeHit(this.transform, damage);
        BreakItem();
    }

    public void OnHitsObject(GameObject other)
    {
        PickUp otherPickUp = other.GetComponent<PickUp>();
        if (otherPickUp.itemState == ItemState.Held)
        {
            otherPickUp.DetachPickUp();
        }
        otherPickUp.BreakItem();
        BreakItem();
    }

    public void OnHitsGround()
    {
        BreakItem();
    }

    public void DetachPickUp()
    {
        Player player = pickUpPlayer.GetComponent<Player>();
        player.pickUpObject = null;
        player.playerState = PlayerState.Idle;
        player.animator.SetBool("isPickingUp", false);
        BreakItem();
    }

    public void BreakItem()
    {
        EventManager.TriggerEvent("ObjectPicked");
        ParticleManager.Instance.PlayEffect(gameObject);
        Destroy(gameObject);
    }

    public void FreezeAll()
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    public void Unfreeze()
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
    }



    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(origin, sphereRadius);
    }

    public void ParentPlayer(GameObject player)
    {
        pickUpPlayer = player;
    }

    internal void ChangeTrails()
    {
        redTrailActive = true;
    }
}

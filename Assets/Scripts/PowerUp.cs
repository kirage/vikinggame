﻿using UnityEngine;
using System.Collections.Generic;

public class PowerUp : MonoBehaviour
{
	public List<IPowerUp> PowerUps = new List<IPowerUp>();

	private Game gameHandler;

	private void Awake()
	{
		gameHandler = GameObject.FindGameObjectWithTag("GameHandler").GetComponent<Game>();

		Heal heal = GetComponent<Heal>();
		ExtraDamage extraDamage = GetComponent<ExtraDamage>();
		SpeedUp speedUp = GetComponent<SpeedUp>();
		Shield shield = GetComponent<Shield>();
		TimedDeath timedDeath = GetComponent<TimedDeath>();

		if(heal != null)
		{
			PowerUps.Add(heal);
		}
		
		if(extraDamage != null)
		{
			PowerUps.Add(GetComponent<ExtraDamage>());
		}

		if(speedUp != null)
		{
			PowerUps.Add(GetComponent<SpeedUp>());
		}

		if(shield != null)
		{
			PowerUps.Add(GetComponent<Shield>());
		}

		if(timedDeath != null)
		{
			PowerUps.Add(GetComponent<TimedDeath>());
		}
	}
	private void OnTriggerEnter(Collider other)
	{
		GameObject player = other.gameObject;
		Player playerScript = player.GetComponent<Player>();

		if (playerScript.activePowerUps.ContainsKey("ExtraDamage"))
		{
			PowerUps.Remove(GetComponent<ExtraDamage>());
		}
		if (playerScript.activePowerUps.ContainsKey("SpeedUp"))
		{
			PowerUps.Remove(GetComponent<SpeedUp>());
		}
		if (playerScript.activePowerUps.ContainsKey("Shield"))
		{
			if (playerScript.activePowerUps["Shield"] == 2)
			{
				PowerUps.Remove(GetComponent<Shield>());
			}
		}

		PowerUps[Random.Range(0, PowerUps.Count)].Execute(player);

		//PowerUps[4].Execute(player);
		gameHandler.powerUpActive = false;
		Destroy(gameObject);
	}
}

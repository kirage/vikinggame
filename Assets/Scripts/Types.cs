﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Get access to enum states by 'using Types;'
namespace Types
{
    public enum Character
    {
        Blue,
        Yellow,
        Brown,
        Purple,
    }

    public enum PlayerState
    {
        Idle,
        Jab,
        Hold,
        Hit,
    }

    public enum Movement
    {
        Stunned,
        Moving,
        Idle,
        Jumping,
        Dead
    }

    public enum GameState
    {
        MainMenu,
        JoinGame,
        Countdown,
        Game,
		Win,
        Testing,
        Victory
    }

    public enum ItemType
    {
        Barrel,
        Bottle,
        Stool
    }

    public enum ItemState
    {
        Ground,
        Held,
        Air
    }

}

﻿using UnityEngine;
using System.Collections;

public class Heal : MonoBehaviour, IPowerUp
{
	[SerializeField]
	private int healAmount = 3;

	private Health health;

	public void Execute(GameObject player)
	{
		health = player.GetComponent<Health>();

		health.IncreaseHealth(healAmount);
		ParticleManager.Instance.PlayEffect(player, "heal");

		Debug.Log("Heal PowerUp");
	}
	public void Reset()
	{
		
	}
}

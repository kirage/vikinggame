﻿using UnityEngine;
using System.Collections;

public class ExtraDamage : MonoBehaviour, IPowerUp
{
	[SerializeField]
	private int extraDamageDealt = 2;

	private Player playerScript;

	public void Execute(GameObject player)
	{
		playerScript = player.GetComponent<Player>();

		ParticleManager.Instance.PlayEffect(player, "strength_pickup");

		playerScript.activePowerUps.Add("ExtraDamage", extraDamageDealt);
		playerScript.ActivateExtraDamageEffect(true);

		Debug.Log("ExtraDamage PowerUp");
	}

	public void Reset()
	{

	}
}

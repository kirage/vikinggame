﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUp : MonoBehaviour, IPowerUp
{
	[SerializeField, Range(0, 1)]
	public float movementSpeedModifier = 0.5f;

	[SerializeField, Range(5, 30)]
	public int buffDuration = 5;

	private Player playerScript;
	private PlayerMovement movementScript;

	public void Execute(GameObject player)
	{
		playerScript = player.GetComponent<Player>();
		movementScript = player.GetComponent<PlayerMovement>();

		playerScript.activePowerUps.Add("SpeedUp", buffDuration);

		movementScript.SetMovementSpeedBuffValues(movementSpeedModifier, buffDuration);
		movementScript.StartSpeedUp();
		//ParticleManager.Instance.PlayEffect(player, "speed_pickup");

		Debug.Log("SpeedUp PowerUp");
	}

	public void Reset()
	{
		throw new System.NotImplementedException();
	}
}

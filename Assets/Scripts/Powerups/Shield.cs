﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour, IPowerUp
{
	private Player playerScript;

	public void Execute(GameObject player)
	{
		playerScript = player.GetComponent<Player>();

		if (playerScript.activePowerUps.ContainsKey("Shield"))
		{
			int value = playerScript.activePowerUps["Shield"];
			playerScript.activePowerUps["Shield"] = value + 1;
		}
		else
		{
			playerScript.activePowerUps.Add("Shield", 1);
		}

		ParticleManager.Instance.PlayEffect(player, "shield_pickup");
		playerScript.ActivateShield();

		Debug.Log("Shield PowerUp " + playerScript.activePowerUps["Shield"]);
	}

	public void Reset()
	{
		throw new System.NotImplementedException();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDeath : MonoBehaviour, IPowerUp
{
	[SerializeField, Range(5,20)]
	private int deathTimer = 5;

	private Player playerScript;

	private Game gameHandler;
	
	public void Execute(GameObject player)
	{
		playerScript = player.GetComponent<Player>();
		gameHandler = GameObject.FindGameObjectWithTag("GameHandler").GetComponent<Game>();

		gameHandler.StartCoroutine(gameHandler.StartTimedDeath(playerScript, deathTimer));
		playerScript.TagWithDeath(true);

	}

	public void Reset()
	{
		throw new System.NotImplementedException();
	}
}

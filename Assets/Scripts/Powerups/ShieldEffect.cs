﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldEffect : MonoBehaviour
{
    [SerializeField]
    private GameObject shieldOne = null;

    [SerializeField]
    private GameObject shieldTwo = null;

    private Player player;

    private void Awake()
    {
        player = GetComponentInParent<Player>();
    }

    public void CheckActiveShields()
    {
        int count = 0;
        if (player.activePowerUps.ContainsKey("Shield"))
        {
            count = player.activePowerUps["Shield"];
        }

        if(count == 2)
        {
            shieldTwo.SetActive(true);
            shieldOne.SetActive(true);
        }
        else if(count == 1)
        {
            shieldOne.SetActive(true);
            shieldTwo.SetActive(false);
            ParticleManager.Instance.PlayEffect(shieldTwo, "shieldBreak");
        }
        else
        {
            shieldOne.SetActive(false);
            shieldTwo.SetActive(false);
            gameObject.SetActive(false);
            ParticleManager.Instance.PlayEffect(shieldOne, "shieldBreak");
        }
    }
}

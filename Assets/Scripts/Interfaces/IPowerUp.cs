﻿
using UnityEngine;

public interface IPowerUp
{
	void Execute(GameObject player);
	void Reset();
}

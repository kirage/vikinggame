﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;
using UnityEngine.Audio;

public class PlayerSounds : MonoBehaviour
{
    public Sound[] sounds;

    [SerializeField]
    public AudioSource currentEffectsSource;

    [SerializeField]
    public float sfxVolume = 5.0f;


    public float LowPitchRange = .95f;
    public float HighPitchRange = 1.05f;

    [SerializeField]
    public bool sfxOn = true;

    // Singleton instance.
    public static AudioManager Instance = null;
    public AudioMixerGroup soundMixer;

    public AudioSource testSource;
    
    void Awake()
    {
        testSource = GetComponent<AudioSource>();
    }

    public void PlaySFX(string name, float delay = 0)
    {
        if (sfxOn)
        {
            foreach (Sound sound in sounds)
            {
                if (sound.name == name)
                {
                    StartCoroutine(WaitUntilPlay(delay, sound));
                }
            }
        }
    }

    IEnumerator WaitUntilPlay(float delay, Sound sound)
    {
        yield return new WaitForSeconds(delay);
        testSource.pitch = Random.Range(LowPitchRange, HighPitchRange);
        testSource.PlayOneShot(sound.clip);
    }

}

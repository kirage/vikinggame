﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Types;

[RequireComponent(typeof(Camera))]
public class MultipleTargetCamera : MonoBehaviour
{
    public List<GameObject> activePlayers;

    public Vector3 offset;
    private Vector3 velocity;
    Vector3 eulers = Vector3.zero;
    public float angle;
    public float smoothTime = 0.5f;
    public float minZoom = 40f;
    public float maxZoom = 10f;
    public float zoomLimiter = 50f;
    private Camera cam;

    //Camera bounds
    public float frontBound = -4f;
    public float backBound = 4f;
    public float sideBounds = 4.5f;

    //Zooming things
    public float zoom;
    public float zoomFactor = 0.5f;
    public int min = 1;
    public int max = 5;

    void Start()
    {
        cam = GetComponent<Camera>();
        if (GameManager.Instance.gameState == GameState.Game || GameManager.Instance.gameState == GameState.Testing || GameManager.Instance.gameState == GameState.Countdown)
        {
            activePlayers = GameManager.Instance.activePlayers;
        }
    }

    void LateUpdate()
    {
        if (activePlayers.Count == 0)
        {
            return;
        }
        if (activePlayers.Count == 1)
        {

            Zoom();
        }

        Move();


        //Zoom by moving cameras distance from centerpoint.
        Vector3 v = GetCenterPoint() - transform.forward * Mathf.Clamp(GetGreatestDistance(), min, max) * zoomFactor;

        transform.position = Vector3.Lerp(transform.position, v, Time.deltaTime);
    }

    void Zoom()
    {
        Vector3 offSet = new Vector3(0, 3f, 0);
        this.transform.rotation = Quaternion.Euler(new Vector3(angle, eulers.y, eulers.z));
        maxZoom = 25f;
        angle = 20f;
        float newZoom = Mathf.Lerp(maxZoom, minZoom, GetGreatestDistance() / zoomLimiter);
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, newZoom, Time.deltaTime);
    }

    void Move()
    {
        Vector3 centerPoint = GetCenterPoint();

        Vector3 v = centerPoint + offset;

        Vector3 newPosition = v;

        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);

    }

    float GetGreatestDistance()
    {
        var bounds = new Bounds(activePlayers[0].transform.position, Vector3.zero);

        for (int i = 0; i < activePlayers.Count; i++)
        {
            bounds.Encapsulate(activePlayers[i].transform.position);
        }

        return bounds.size.x;
    }

    Vector3 GetCenterPoint()
    {
        if (activePlayers.Count == 1)
        {
            return activePlayers[0].transform.position;
        }

        var bounds = new Bounds(activePlayers[0].transform.position, Vector3.zero);
        for (int i = 0; i < activePlayers.Count; i++)
        {
            bounds.Encapsulate(activePlayers[i].transform.position);
        }

        Vector3 offSet = new Vector3(0, 3f, 0);

        Vector3 centerPos = bounds.center + offSet;

        if (centerPos.z < frontBound)
        {
            centerPos.z = frontBound;
        }
        if(centerPos.z > backBound)
        {
            centerPos.z = backBound;
        }
        if (centerPos.x < (-sideBounds))
        {
            centerPos.x = -sideBounds;
        }
        if (centerPos.x > sideBounds)
        {
            centerPos.x = sideBounds;
        }

        return centerPos;
    }
}




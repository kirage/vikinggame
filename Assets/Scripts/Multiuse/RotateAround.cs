﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
    [SerializeField]
    private Vector3 axisRotation = Vector3.zero;

    [SerializeField] 
    private RotationSpace space = RotationSpace.World;

    private enum RotationSpace
    {
        World,
        Self
    }

    void Update()
    {
        if(space == RotationSpace.World)
        {
            transform.Rotate(axisRotation * Time.deltaTime, Space.World);
        }
        else if(space == RotationSpace.Self)
        {
            transform.Rotate(axisRotation * Time.deltaTime, Space.Self);
        }
    }
}

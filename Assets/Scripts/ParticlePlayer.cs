﻿using UnityEngine;

public class ParticlePlayer : MonoBehaviour
{
    ParticleSystem[] parts;
    MeshRenderer[] meshes;

    // Find rferences to meshes and particle systems in object.
    void Start()
    {
        parts = gameObject.GetComponentsInChildren<ParticleSystem>();
        meshes = gameObject.GetComponentsInChildren<MeshRenderer>();
    }

    public void PlayAnimations()
    {
        // Hide all meshes in gameobject.
        foreach (var item in meshes)
        {
            item.enabled = false;
        }
        // Play all particle systems in gameobject.
        foreach (ParticleSystem stuff in parts)
        {
            stuff.Play();
        }
    }
}
